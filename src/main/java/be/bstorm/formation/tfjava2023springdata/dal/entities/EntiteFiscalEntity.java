package be.bstorm.formation.tfjava2023springdata.dal.entities;

import be.bstorm.formation.tfjava2023springdata.dal.embedded.Adresse;
import jakarta.persistence.*;
import lombok.Cleanup;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity(name = "EntiteFiscal")
@Table(name = "entite_fiscal")
@Inheritance(strategy = InheritanceType.JOINED)
@Data
@EntityListeners(value = { AuditingEntityListener.class })
public class EntiteFiscalEntity extends BaseEntity {
    @Column(nullable = false)
    private String nom;
    @Column(nullable = false)
    private String telephone;

    @Embedded
    private Adresse adresse;
}
