package be.bstorm.formation.tfjava2023springdata.dal.entities;

import be.bstorm.formation.tfjava2023springdata.dal.entities.fiscal.PiloteEntity;
import jakarta.persistence.*;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.io.Serializable;

@Entity(name = "Vol")
@Table(name = "vol")
@Data
public class VolRelation extends BaseRelation<VolRelation.VolId> {
    private int nbHeure;

    @ManyToOne(targetEntity = PiloteEntity.class)
    @MapsId(value = "pilotId")
    private PiloteEntity pilote;
    @ManyToOne(targetEntity = TypeAvionEntity.class)
    @MapsId(value = "typeId")
    private TypeAvionEntity type;

    @Embeddable
    @Data
    public static class VolId implements Serializable {
        private Long pilotId;
        private Long typeId;
    }
}
