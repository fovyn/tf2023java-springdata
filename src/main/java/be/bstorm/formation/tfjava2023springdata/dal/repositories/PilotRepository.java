package be.bstorm.formation.tfjava2023springdata.dal.repositories;

import be.bstorm.formation.tfjava2023springdata.dal.entities.fiscal.PiloteEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PilotRepository extends JpaRepository<PiloteEntity, Long> {
}
