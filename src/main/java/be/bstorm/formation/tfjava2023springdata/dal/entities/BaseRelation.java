package be.bstorm.formation.tfjava2023springdata.dal.entities;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;

import java.io.Serializable;

@MappedSuperclass
public abstract class BaseRelation<TKey extends Serializable> {
    @EmbeddedId
    private TKey id;
}
