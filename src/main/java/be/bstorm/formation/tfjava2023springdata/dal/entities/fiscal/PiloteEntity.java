package be.bstorm.formation.tfjava2023springdata.dal.entities.fiscal;

import be.bstorm.formation.tfjava2023springdata.dal.entities.EntiteFiscalEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity(name = "Pilote")
@Data
@ToString(callSuper = true)
@EntityListeners(value = { AuditingEntityListener.class })
public class PiloteEntity extends EntiteFiscalEntity {
    @Column(nullable = false, unique = true)
    private String brevet;


}
