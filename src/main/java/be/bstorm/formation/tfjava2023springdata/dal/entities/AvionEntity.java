package be.bstorm.formation.tfjava2023springdata.dal.entities;

import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDate;

@Entity(name = "Avion")
@Table(name = "avion")
@Data
@EqualsAndHashCode(callSuper = false)
@EntityListeners(value = { AuditingEntityListener.class })
public class AvionEntity extends BaseEntity {
    @Column(nullable = false)
    private String imma;
    private LocalDate dateAchat;

    @ManyToOne(targetEntity = TypeAvionEntity.class)
    private TypeAvionEntity type;
    @ManyToOne(targetEntity = EntiteFiscalEntity.class)
    private EntiteFiscalEntity owner;
}
