package be.bstorm.formation.tfjava2023springdata.dal.repositories;

import be.bstorm.formation.tfjava2023springdata.dal.entities.EntiteFiscalEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EntiteFiscalRepository extends JpaRepository<EntiteFiscalEntity, Long> {

    @Query(value = "SELECT ef FROM Avion a JOIN a.owner ef")
    List<EntiteFiscalEntity> findAllProprietaire();

    @Query(value = "SELECT ef FROM EntiteFiscal ef WHERE ef.adresse.rue = :rue")
    List<EntiteFiscalEntity> findAllByAdresseRue(@Param("rue") String rue);
}
