package be.bstorm.formation.tfjava2023springdata.dal.entities;

import be.bstorm.formation.tfjava2023springdata.dal.entities.fiscal.MecanicienEntity;
import jakarta.persistence.*;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;

@Entity(name = "Intervention")
@Table(name = "intervention")
@Data
@EntityListeners(value = { AuditingEntityListener.class })
public class InterventionEntity extends BaseEntity {
    @Column(nullable = false)
    private String objet;
    @Column(nullable = false)
    private LocalDateTime dateExecution;

    @ManyToOne(targetEntity = AvionEntity.class, optional = false)
    private AvionEntity avion;
    @ManyToOne(targetEntity = MecanicienEntity.class, optional = false)
    private MecanicienEntity verificateur;
    @ManyToOne(targetEntity = MecanicienEntity.class, optional = false)
    private MecanicienEntity reparateur;
}
