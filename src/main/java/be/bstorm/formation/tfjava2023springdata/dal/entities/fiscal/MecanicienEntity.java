package be.bstorm.formation.tfjava2023springdata.dal.entities.fiscal;

import be.bstorm.formation.tfjava2023springdata.dal.entities.EntiteFiscalEntity;
import be.bstorm.formation.tfjava2023springdata.dal.entities.TypeAvionEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.OneToMany;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.util.List;

@Entity(name = "Mecanicien")
@Data
@EntityListeners(value = { AuditingEntityListener.class })
public class MecanicienEntity extends EntiteFiscalEntity {

    @OneToMany(targetEntity = TypeAvionEntity.class)
    private List<TypeAvionEntity> types;
}
