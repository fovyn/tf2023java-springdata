package be.bstorm.formation.tfjava2023springdata.dal.repositories;

import be.bstorm.formation.tfjava2023springdata.dal.entities.AvionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AvionRepository extends JpaRepository<AvionEntity, Long> {

    @Query(value = "SELECT a FROM Avion a JOIN FETCH a.owner")
    List<AvionEntity> findAllWithOwner();
}
