package be.bstorm.formation.tfjava2023springdata.dal.embedded;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.Data;

@Embeddable
@Data
public class Adresse {
    @Column(name="adresse_rue", nullable = false)
    private String rue;
    @Column(name="adresse_numero", nullable = false)
    private String numero;
    @Column(name="adresse_localite", nullable = false)
    private String localite;
    @Column(name="adresse_cp", nullable = false)
    private String cp;
    @Column(name="adresse_pays", nullable = false)
    private String pays;
}
