package be.bstorm.formation.tfjava2023springdata.dal.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.Table;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity(name = "TypeAvion")
@Table(name = "type_avion")
@Data
@EntityListeners(value = { AuditingEntityListener.class })
public class TypeAvionEntity extends BaseEntity {
    @Column(nullable = false)
    private String nom;
    @Column(nullable = false)
    private String constructeur;
    @Column(nullable = false)
    private float puissance;
    @Column(nullable = false)
    private int place;
}
