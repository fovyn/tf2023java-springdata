package be.bstorm.formation.tfjava2023springdata.dal.listeners;

import be.bstorm.formation.tfjava2023springdata.dal.listeners.annotation.EmpruntDuration;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

public class EmpruntListener {

    @PrePersist
    public void prePersistListener(Object target) {
        List<Field> durationList = Arrays.stream(target.getClass().getDeclaredFields())
                .filter(it -> it.isAnnotationPresent(EmpruntDuration.class))
                .toList();

        try {
            Field empruntAt = target.getClass().getDeclaredField("empruntAt");
            Field bo = target.getClass().getDeclaredField("empruntAt");
            LocalDate value = (LocalDate) empruntAt.get(target);

            durationList.forEach(it -> {
                EmpruntDuration duration = it.getAnnotation(EmpruntDuration.class);
                try {
                    it.set(value.plusDays(duration.value()), target);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            });
        } catch (Exception e) {

        }

    }
}
