package be.bstorm.formation.tfjava2023springdata.pl.models;

import jakarta.validation.constraints.NotBlank;

public record FPilotCreate(
        @NotBlank String nom,
        @NotBlank String telephone,
        String adresse,
        String brevet
) {
}
