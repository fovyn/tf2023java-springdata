package be.bstorm.formation.tfjava2023springdata.pl.controllers;

import be.bstorm.formation.tfjava2023springdata.dal.repositories.EntiteFiscalRepository;
import be.bstorm.formation.tfjava2023springdata.dal.repositories.PilotRepository;
import be.bstorm.formation.tfjava2023springdata.pl.models.FPilotCreate;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.stream.Collectors;

@Controller
@RequestMapping(path = {"/pilots"})
public class PilotController {
    private final PilotRepository pilotRepository;
    private final EntiteFiscalRepository repository;

    @Autowired
    public PilotController(PilotRepository pilotRepository, EntiteFiscalRepository repository) {
        this.pilotRepository = pilotRepository;
        this.repository = repository;
    }



    @GetMapping(path = {"", "/"})
    public String createAction(Model model) {
        model.addAttribute("errors", new HashMap<>());
        model.addAttribute("proprietaire", repository.findAllProprietaire());
        return "pilots/create";
    }

    @PostMapping(path = {"", "/"})
    public String createAction(
            Model model,
            @Valid FPilotCreate form,
            BindingResult bindingResult
    ) {
//        Map<String, List<String>> errors = new HashMap<>();
//        if (form.nom() == null) {
//            if (!errors.containsKey("nom")) {
//                errors.put("nom", new ArrayList<>());
//            }
//            errors.get("nom").add("Field cannot be null");
//        }
//        if (form.nom().length() == 0) {
//            if (!errors.containsKey("nom")) {
//                errors.put("nom", new ArrayList<>());
//            }
//            errors.get("nom").add("Field cannot be empty");
//        }
//
//        if (!errors.isEmpty()) {
//            model.addAttribute("errors", errors);
//            return "pilots/create";
//        }

        if (bindingResult.hasErrors()) {
            model.addAttribute(
                    "errors",
                    bindingResult.getFieldErrors().stream().collect(Collectors.groupingBy(FieldError::getField))
            );
            return "pilots/create";
        }

        return "redirect:/pilots";
    }
}
