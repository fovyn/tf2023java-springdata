package be.bstorm.formation.tfjava2023springdata.pl.controllers;

import be.bstorm.formation.tfjava2023springdata.dal.entities.AvionEntity;
import be.bstorm.formation.tfjava2023springdata.dal.entities.EntiteFiscalEntity;
import be.bstorm.formation.tfjava2023springdata.dal.repositories.AvionRepository;
import be.bstorm.formation.tfjava2023springdata.dal.repositories.EntiteFiscalRepository;
import be.bstorm.formation.tfjava2023springdata.pl.models.avions.FAvionCreate;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping(path = {"/avions"})
public class AvionController {
    public static final String REDIRECT_TO_LIST = "redirect:/avions";
    private final AvionRepository avionRepository;
    private final EntiteFiscalRepository entiteFiscalRepository;

    @Autowired
    public AvionController(AvionRepository avionRepository, EntiteFiscalRepository entiteFiscalRepository) {
        this.avionRepository = avionRepository;
        this.entiteFiscalRepository = entiteFiscalRepository;
    }

    @GetMapping(path = {"", "/"})
    public String indexAction(Model model) {
        List<AvionEntity> avions = this.avionRepository.findAllWithOwner();
        model.addAttribute("avions", avions);

        return "avions/index";
    }

    @GetMapping(path = {"/create", "/add"})
    public String createAction(Model model) {
        List<EntiteFiscalEntity> entities = this.entiteFiscalRepository.findAll();
        model.addAttribute("owners", entities);
        model.addAttribute("errors", new HashMap<>());

        return "avions/create";
    }

    @PostMapping(path = {"/create", "/add"})
    public String createAction(
            Model model,
            @Valid FAvionCreate form,
            BindingResult bindingResult
    ) {

        List<EntiteFiscalEntity> entities = this.entiteFiscalRepository.findAll();
        model.addAttribute("owners", entities);
        if (bindingResult.hasErrors()) {
            Map<String, List<FieldError>> errors = bindingResult.getFieldErrors().stream().collect(Collectors.groupingBy(FieldError::getField));
            model.addAttribute("errors", errors);
            return "avions/create";
        }

        Optional<EntiteFiscalEntity> owner = this.entiteFiscalRepository.findById(form.ownerId());
        if (owner.isEmpty()) {
            return "errors/404";
        }

        AvionEntity avion = form.toEntity();
        avion.setOwner(owner.get());

        this.avionRepository.save(avion);

        return REDIRECT_TO_LIST;
    }

//    @GetMapping(path = {"/{id}", "/{id}/update"})
//    public String updateAction(
//            Model model,
//            @PathVariable("id") Long id
//    ) {
//        Optional<AvionEntity> opt = this.avionRepository.findById(id);
//
//        if (opt.isPresent()) {
//            AvionEntity avion = opt.get();
//            model.addAttribute("avion", avion);
//
//            return "avions/update";
//        }
//
//        return "error/404";
//    }
//
//    @PostMapping(path = {"/{id}", "/{id}/update"})
//    public String updateAction(
//            Model model,
//            @PathVariable("id") Long id,
//            @Valid FAvionUpdate form
//    ) {
//
//        return REDIRECT_TO_LIST;
//    }

    @GetMapping(path = "/remove/{id}")
    public String removeAction(
            Model model,
            @PathVariable("id") Long id
    ) {

        return REDIRECT_TO_LIST;
    }
}
