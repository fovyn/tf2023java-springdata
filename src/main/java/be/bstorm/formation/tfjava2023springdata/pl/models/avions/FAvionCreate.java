package be.bstorm.formation.tfjava2023springdata.pl.models.avions;

import be.bstorm.formation.tfjava2023springdata.dal.entities.AvionEntity;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;

import java.time.LocalDate;

public record FAvionCreate(
        @NotBlank
        String imma,
        @NotBlank
        String dateAchat,
        @Min(value = 1L)
        long ownerId
) {

    public AvionEntity toEntity() {
        AvionEntity entity = new AvionEntity();

        entity.setImma(imma);
        entity.setDateAchat(LocalDate.parse(dateAchat));

        return entity;
    }
}
