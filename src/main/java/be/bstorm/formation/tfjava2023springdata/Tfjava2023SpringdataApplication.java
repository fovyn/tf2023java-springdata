package be.bstorm.formation.tfjava2023springdata;

import be.bstorm.formation.tfjava2023springdata.dal.embedded.Adresse;
import be.bstorm.formation.tfjava2023springdata.dal.entities.fiscal.PiloteEntity;
import be.bstorm.formation.tfjava2023springdata.dal.repositories.PilotRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class Tfjava2023SpringdataApplication implements CommandLineRunner {
    private final PilotRepository pilotRepository;

    public Tfjava2023SpringdataApplication(PilotRepository pilotRepository) {
        this.pilotRepository = pilotRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(Tfjava2023SpringdataApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
    }
}
